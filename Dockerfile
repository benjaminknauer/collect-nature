FROM amazoncorretto:18-alpine

COPY backend/target/collect-nature-backend-0.0.1-SNAPSHOT.jar /app.jar

EXPOSE 8080
WORKDIR /
ENV ENV=prod
ENTRYPOINT ["java", "-jar", "/app.jar"]