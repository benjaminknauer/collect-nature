create table if not exists benutzer
(
    id           int auto_increment
        primary key,
    benutzername varchar(255) not null,
    passwort     varchar(255) not null,
    bio          text         null,
    constraint user_username_uindex
        unique (benutzername)
);

create table if not exists folgen
(
    benutzerId           int not null,
    verfolgterBenutzerId int not null,
    primary key (benutzerId, verfolgterBenutzerId),
    constraint folgen_benutzer_id_fk
        foreign key (benutzerId) references benutzer (id),
    constraint folgen_benutzer_id_fk_2
        foreign key (verfolgterBenutzerId) references benutzer (id)
);

create table if not exists fund
(
    id           int auto_increment
        primary key,
    bildUrl      varchar(255) null,
    fundname     varchar(255) null,
    beschreibung text         null,
    datum        varchar(255) null,
    ort          varchar(255) null,
    benutzerId   int          null,
    constraint fund_benutzer_id_fk
        foreign key (benutzerId) references benutzer (id)
);

create table if not exists gefaellt
(
    benutzerId int not null,
    fundId     int not null,
    primary key (benutzerId, fundId),
    constraint like_benutzer_id_fk
        foreign key (benutzerId) references benutzer (id),
    constraint like_fund_id_fk
        foreign key (fundId) references fund (id)
);