create table benutzer
(
    id           INTEGER not null
        constraint user_pk
            primary key autoincrement,
    benutzername varchar not null,
    passwort     varchar not null,
    bio          TEXT
);

create unique index user_username_uindex
    on benutzer (benutzername);

create table folgen
(
    benutzerId           INTEGER not null,
    verfolgterBenutzerId INTEGER not null,
    constraint folgen_pk
        primary key (verfolgterBenutzerId, benutzerId),
    constraint folgen_user_id_id_fk
        foreign key (benutzerId, verfolgterBenutzerId) references benutzer (id, id)
);

create table fund
(
    id           INTEGER not null
        constraint fund_pk
            primary key autoincrement,
    bildUrl      VARCHAR,
    fundname     VARCHAR,
    beschreibung TEXT,
    datum        VARCHAR,
    ort          VARCHAR,
    benutzerId   INTEGER
        references benutzer
);

create table gefaellt
(
    benutzerId INTEGER not null
        references benutzer,
    fundId     INTEGER not null
        references fund,
    constraint like_pk
        primary key (benutzerId, fundId)
);
