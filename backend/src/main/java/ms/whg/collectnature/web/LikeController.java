package ms.whg.collectnature.web;

import ms.whg.collectnature.db.LikeRepository;
import ms.whg.collectnature.web.security.util.LoginManager;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class LikeController {

    LikeRepository likeRepository = new LikeRepository();

    @PostMapping("/like/{fundId}")
    public ResponseEntity<Void> like(@PathVariable int fundId) {
        int benutzerId = LoginManager.getCurrentBenutzer().getId();
        likeRepository.like(benutzerId, fundId);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/unlike/{fundId}")
    public ResponseEntity<Void> unlike(@PathVariable int fundId) {
        int benutzerId = LoginManager.getCurrentBenutzer().getId();
        likeRepository.unlike(benutzerId, fundId);
        return ResponseEntity.ok().build();
    }

}
