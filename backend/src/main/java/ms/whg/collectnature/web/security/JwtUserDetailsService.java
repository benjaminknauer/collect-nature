package ms.whg.collectnature.web.security;

import ms.whg.collectnature.db.BenutzerRepository;
import ms.whg.collectnature.db.entity.BenutzerEntity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class JwtUserDetailsService implements UserDetailsService {

    BenutzerRepository benutzerRepository = new BenutzerRepository();
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Override
    public UserWithId loadUserByUsername(String pBenutzername) throws UsernameNotFoundException {
        BenutzerEntity benutzer = benutzerRepository.findByBenutzername(pBenutzername)
                .orElseThrow(() -> new UsernameNotFoundException("Username " + pBenutzername + " nicht gefunden."));

        return new UserWithId(benutzer.getId(), benutzer.getBenutzername(), benutzer.getPasswort());
    }

    public void createUser(String benutzername, String password) {
        String passwordHash = passwordEncoder.encode(password);
        benutzerRepository.insertBenutzer(new BenutzerEntity(null, benutzername, passwordHash));
    }
}