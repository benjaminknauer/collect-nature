package ms.whg.collectnature.web.security.util;

import ms.whg.collectnature.db.entity.BenutzerEntity;
import ms.whg.collectnature.web.security.UserWithId;
import org.springframework.security.core.context.SecurityContextHolder;

public class LoginManager {

    public static BenutzerEntity getCurrentBenutzer() {
        UserWithId user = (UserWithId) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        return new BenutzerEntity(user.getUserId(), user.getUsername(), user.getPassword());
    }

}
