package ms.whg.collectnature.web;

import ms.whg.collectnature.db.entity.BenutzerEntity;
import ms.whg.collectnature.web.security.util.LoginManager;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class ImgUploadController {

    @PostMapping("/image/upload")
    public ResponseEntity<ImgUploadResponse> uploadImage(@RequestParam("image") MultipartFile multipartFile) throws IOException {

        BenutzerEntity benutzer = LoginManager.getCurrentBenutzer();

        String fileName = UUID.randomUUID() + "_" + StringUtils.cleanPath(multipartFile.getOriginalFilename());
        String uploadDir = "user-photos/" + benutzer.getId();

        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }

        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);

            String urlPath = "/" + uploadDir + "/" + fileName;
            return ResponseEntity.ok(new ImgUploadResponse(urlPath));

        } catch (IOException ioe) {
            throw new IOException("Could not save image file: " + fileName, ioe);
        }

    }

}
