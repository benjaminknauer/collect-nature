package ms.whg.collectnature.web;

import lombok.Data;

@Data
public class RegisterRequest {
    String benutzername;
    String password;
}
