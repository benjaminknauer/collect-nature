package ms.whg.collectnature.web;

import ms.whg.collectnature.db.BenutzerRepository;
import ms.whg.collectnature.db.aggregate.BenutzernameAndBenutzerId;
import ms.whg.collectnature.db.entity.BenutzerEntity;
import ms.whg.collectnature.web.security.util.LoginManager;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class FreundeSuchenController {

    BenutzerRepository userRepository = new BenutzerRepository();

    @PostMapping("/freunde-suchen")
    public ResponseEntity<List<FreundeSuchenResponse>> freundeSuchen(@RequestBody FreundeSuchenRequest request) {
        BenutzerEntity currentBenutzer = LoginManager.getCurrentBenutzer();
        List<BenutzernameAndBenutzerId> users = List.of(
                userRepository.findAllByBenutzernameFuzzy(request.suchString, currentBenutzer.getId())
        );

        List<FreundeSuchenResponse> responseUsers = new ArrayList<>();
        for (BenutzernameAndBenutzerId user : users) {
            responseUsers.add(new FreundeSuchenResponse(user.getBenutzerId(), user.getBenutzername()));
        }

        return ResponseEntity.ok(responseUsers);
    }

    @GetMapping("/freunde")
    public ResponseEntity<BenutzernameAndBenutzerId[]> freunde() {
        BenutzerEntity currentBenutzer = LoginManager.getCurrentBenutzer();
        var freunde = userRepository.findFreunde(currentBenutzer.getId());

        return ResponseEntity.ok(freunde);
    }

    @PostMapping("/folgen/{benutzerId}")
    public ResponseEntity<Void> folgen(@PathVariable int benutzerId) {
        BenutzerEntity currentBenutzer = LoginManager.getCurrentBenutzer();
        userRepository.folgen(currentBenutzer.getId(), benutzerId);

        return ResponseEntity.ok().build();
    }

    @PostMapping("/entfolgen/{benutzerId}")
    public ResponseEntity<Void> entfolgen(@PathVariable int benutzerId) {
        BenutzerEntity currentBenutzer = LoginManager.getCurrentBenutzer();
        userRepository.entfolgen(currentBenutzer.getId(), benutzerId);

        return ResponseEntity.ok().build();
    }

}
