package ms.whg.collectnature.web;

import ms.whg.collectnature.db.GefaelltRepository;
import ms.whg.collectnature.db.FundRepository;
import ms.whg.collectnature.db.BenutzerRepository;
import ms.whg.collectnature.db.aggregate.FundEntityWithBenutzerDetails;
import ms.whg.collectnature.db.entity.BenutzerEntity;
import ms.whg.collectnature.web.security.util.LoginManager;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ms.whg.collectnature.db.entity.FundEntity;

import java.util.ArrayList;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class FundeController {

    FundRepository fundRepository = new FundRepository();
    BenutzerRepository benutzerRepositoy = new BenutzerRepository();
    GefaelltRepository gefaelltRepository = new GefaelltRepository();

    @GetMapping({"/funde/me", "/funde/me/{idOfLastDisplayedFund}"})
    public ResponseEntity<FundResponse> funde(
            @PathVariable(required = false) Integer idOfLastDisplayedFund
    ) {
        BenutzerEntity currentBenutzer = LoginManager.getCurrentBenutzer();

        List<FundResponse.Fund> funde = new ArrayList<>();
        FundEntity[] fundEntities = idOfLastDisplayedFund == null
                ? fundRepository.findAllByBenutzerId(currentBenutzer.getId())
                : fundRepository.findAllByBenutzerIdPaginated(currentBenutzer.getId(), idOfLastDisplayedFund);

        for (FundEntity fundEntity : fundEntities) {
            int gefaelltCount = gefaelltRepository.getGefaelltCount(fundEntity.getId());
            boolean isGefaellt = gefaelltRepository.isGefaellt(currentBenutzer.getId(), fundEntity.getId());
            Integer eigeneBewertung = gefaelltRepository.getBewertung(currentBenutzer.getId(), fundEntity.getId());
            double avgBewertung = gefaelltRepository.getAvgBewertung(fundEntity.getId());
            funde.add(FundResponse.Fund.builder()
                    .fundId(fundEntity.getId())
                    .bildUrl(fundEntity.getBildUrl())
                    .name(fundEntity.getFundname())
                    .beschreibung(fundEntity.getBeschreibung())
                    .datum(fundEntity.getDatum())
                    .ort(fundEntity.getOrt())
                    .gefaelltCount(gefaelltCount)
                    .gefaellt(isGefaellt)
                    .avgBewertung(avgBewertung)
                    .myBewertung(eigeneBewertung)
                    .build());
        }

        return ResponseEntity.ok(FundResponse.builder()
                .benutzerId(currentBenutzer.getId())
                .benutzername(currentBenutzer.getBenutzername())
                .funde(funde)
                .build());
    }

    @GetMapping({"/funde/{pBenutzerId}", "/funde/{pBenutzerId}/{idOfLastDisplayedFund}"})
    public ResponseEntity<FundResponse> funde(
            @PathVariable() Integer pBenutzerId,
            @PathVariable(required = false) Integer idOfLastDisplayedFund
    ) {
        BenutzerEntity currentBenutzer = LoginManager.getCurrentBenutzer();
        BenutzerEntity benutzerToFindFundeFrom = benutzerRepositoy.findById(pBenutzerId).orElseThrow();

        List<FundResponse.Fund> funde = new ArrayList<>();
        FundEntity[] fundEntities = idOfLastDisplayedFund == null
                ? fundRepository.findAllByBenutzerId(benutzerToFindFundeFrom.getId())
                : fundRepository.findAllByBenutzerIdPaginated(benutzerToFindFundeFrom.getId(), idOfLastDisplayedFund);

        for (FundEntity fundEntity : fundEntities) {
            int gefaelltCount = gefaelltRepository.getGefaelltCount(fundEntity.getId());
            boolean isGefaellt = gefaelltRepository.isGefaellt(currentBenutzer.getId(), fundEntity.getId());
            Integer eigeneBewertung = gefaelltRepository.getBewertung(currentBenutzer.getId(), fundEntity.getId());
            double avgBewertung = gefaelltRepository.getAvgBewertung(fundEntity.getId());
            funde.add(FundResponse.Fund.builder()
                    .fundId(fundEntity.getId())
                    .bildUrl(fundEntity.getBildUrl())
                    .name(fundEntity.getFundname())
                    .beschreibung(fundEntity.getBeschreibung())
                    .datum(fundEntity.getDatum())
                    .ort(fundEntity.getOrt())
                    .gefaelltCount(gefaelltCount)
                    .gefaellt(isGefaellt)
                    .avgBewertung(avgBewertung)
                    .myBewertung(eigeneBewertung)
                    .build());
        }

        return ResponseEntity.ok(FundResponse.builder()
                .benutzerId(benutzerToFindFundeFrom.getId())
                .benutzername(benutzerToFindFundeFrom.getBenutzername())
                .funde(funde)
                .build());
    }

    @GetMapping({"/timeline", "/timeline/{idOfLastDisplayedFund}"})
    public ResponseEntity<TimelineResponse> timeline(@PathVariable(required = false) Integer idOfLastDisplayedFund) {
        BenutzerEntity currentBenutzer = LoginManager.getCurrentBenutzer();

        List<TimelineResponse.Fund> funde = new ArrayList<>();
        FundEntityWithBenutzerDetails[] fundEntities = idOfLastDisplayedFund == null
                ? fundRepository.findTimelineRelated(currentBenutzer.getId())
                : fundRepository.findTimelineRelatedPaginated(currentBenutzer.getId(), idOfLastDisplayedFund);

        for (FundEntityWithBenutzerDetails fundEntity : fundEntities) {
            FundEntity fund = fundEntity.getFund();
            int gefaelltCount = gefaelltRepository.getGefaelltCount(fund.getId());
            Integer eigeneBewertung = gefaelltRepository.getBewertung(currentBenutzer.getId(), fund.getId());
            double avgBewertung = gefaelltRepository.getAvgBewertung(fund.getId());
            funde.add(new TimelineResponse.Fund(
                    fundEntity.getBenutzerId(),
                    fundEntity.getBenutzername(),
                    fund.getId(),
                    fund.getBildUrl(),
                    fund.getFundname(),
                    fund.getBeschreibung(),
                    fund.getDatum(),
                    fund.getOrt(),
                    fundEntity.isGefaellt(),
                    gefaelltCount,
                    avgBewertung,
                    eigeneBewertung
            ));
        }

        return ResponseEntity.ok(TimelineResponse.builder()
                .funde(funde)
                .build());
    }

    @PostMapping("/fund")
    public ResponseEntity<Void> createFunde(@RequestBody FundAnlegenRequest request) {
        int currentBenutzerId = LoginManager.getCurrentBenutzer().getId();
        fundRepository.insert(
                new FundEntity(
                        null,
                        request.getBildUrl(),
                        request.getName(),
                        request.getBeschreibung(),
                        request.getDatum(),
                        request.getOrt(),
                        currentBenutzerId
                )
        );

        return ResponseEntity.ok().build();
    }

}
