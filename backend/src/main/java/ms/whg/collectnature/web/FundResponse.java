package ms.whg.collectnature.web;

import lombok.Builder;
import lombok.Value;
import org.springframework.lang.Nullable;

import java.util.List;

@Value
@Builder
public class FundResponse {
    int benutzerId;
    String benutzername;
    List<Fund> funde;

    @Value
    @Builder
    public static class Fund {
        int fundId;
        String bildUrl;
        String name;
        String beschreibung;
        String datum;
        String ort;
        boolean gefaellt;
        int gefaelltCount;
        double avgBewertung;
        @Nullable
        Integer myBewertung;
    }
}

