package ms.whg.collectnature.web;

import lombok.Builder;
import lombok.Value;
import org.springframework.lang.Nullable;

import java.util.List;

@Value
@Builder
public class TimelineResponse {
    List<Fund> funde;

    @Value
    @Builder
    public static class Fund {
        int benutzerId;
        String benutzername;
        int fundId;
        String bildUrl;
        String name;
        String beschreibung;
        String datum;
        String ort;
        boolean gefaellt;
        int gefaelltCount;
        double avgBewertung;
        @Nullable
        Integer myBewertung;
    }
}
