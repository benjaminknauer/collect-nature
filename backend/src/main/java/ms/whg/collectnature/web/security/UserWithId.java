package ms.whg.collectnature.web.security;

import org.springframework.security.core.userdetails.User;

import java.util.List;

public class UserWithId extends User {

    private final Integer userId;

    public UserWithId(Integer userId, String benutzername, String password) {
        super(benutzername, password, List.of());
        this.userId = userId;
    }

    public Integer getUserId() {
        return this.userId;
    }

}
