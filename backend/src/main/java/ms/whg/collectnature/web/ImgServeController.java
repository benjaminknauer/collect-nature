package ms.whg.collectnature.web;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.net.ssl.HttpsURLConnection;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Path;

@Controller
@RequestMapping("/api")
public class ImgServeController {

    @GetMapping("/user-photos/{benutzerId}/{imgName}")
    @ResponseBody
    public ResponseEntity<?> getImage(@PathVariable String benutzerId, @PathVariable String imgName) throws IOException {
        String env = System.getenv("ENV");
        Path imgPath = Path.of("user-photos/" + benutzerId + "/" + imgName);
        InputStream in;

        boolean proxyImages = env != null && env.equals("dev-prod");
        if (proxyImages) {
            URL url = new URL("https://collect-nature.benjaminknauer.de/api/user-photos/" + benutzerId + "/" + imgName);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            in = con.getInputStream();
        } else {
            boolean isFileExists = imgPath.toFile().exists();
            boolean isFileAllowed = imgPath.toFile().getParentFile().getParentFile().getName().equals("user-photos");
            if (!isFileExists || !isFileAllowed) {
                return ResponseEntity.notFound().build();
            }
            in = new FileInputStream(imgPath.toFile());
        }

        return ResponseEntity.ok()
                .contentType(MediaType.IMAGE_JPEG)
                .header("Cache-Control", "public, max-age=1209600")
                .body(new InputStreamResource(in));
    }

}
