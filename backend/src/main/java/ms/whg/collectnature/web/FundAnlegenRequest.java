package ms.whg.collectnature.web;

import lombok.Data;

@Data
public class FundAnlegenRequest {
    String name;
    String bildUrl;
    String beschreibung;
    String datum;
    String ort;
}
