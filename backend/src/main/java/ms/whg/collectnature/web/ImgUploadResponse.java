package ms.whg.collectnature.web;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ImgUploadResponse {
    String path;
}
