package ms.whg.collectnature.web;

import ms.whg.collectnature.db.GefaelltRepository;
import ms.whg.collectnature.web.security.util.LoginManager;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class GefaelltController {

    GefaelltRepository gefaelltRepository = new GefaelltRepository();

    @PostMapping("/gefaellt/{fundId}")
    public ResponseEntity<Void> gefaellt(@PathVariable int fundId) {
        int benutzerId = LoginManager.getCurrentBenutzer().getId();
        gefaelltRepository.insertGefaellt(benutzerId, fundId, null);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/gefaellt-nicht-mehr/{fundId}")
    public ResponseEntity<Void> gefaelltNichtMehr(@PathVariable int fundId) {
        int benutzerId = LoginManager.getCurrentBenutzer().getId();
        gefaelltRepository.deleteGefaellt(benutzerId, fundId);
        return ResponseEntity.ok().build();
    }

}
