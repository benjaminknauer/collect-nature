package ms.whg.collectnature.web;

import ms.whg.collectnature.db.GefaelltRepository;
import ms.whg.collectnature.web.security.util.LoginManager;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class BewertungController {

    GefaelltRepository gefaelltRepository = new GefaelltRepository();

    @PostMapping("/rating/{fundId}/{bewertung}")
    public ResponseEntity<Void> bewerten(@PathVariable int fundId, @PathVariable int bewertung) {
        int benutzerId = LoginManager.getCurrentBenutzer().getId();
        gefaelltRepository.deleteGefaellt(benutzerId, fundId);
        gefaelltRepository.insertGefaellt(benutzerId, fundId, bewertung);

        return ResponseEntity.ok().build();
    }

    @GetMapping("/rating/{fundId}/avg")
    public ResponseEntity<Double> getAvgBewertung(@PathVariable int fundId) {
        double avgBewertung = gefaelltRepository.getAvgBewertung(fundId);
        return ResponseEntity.ok().body(avgBewertung);
    }

}
