package ms.whg.collectnature.web;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FreundeSuchenResponse {
    int benutzerId;
    String benutzername;
}
