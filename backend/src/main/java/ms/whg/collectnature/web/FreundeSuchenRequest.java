package ms.whg.collectnature.web;

import lombok.Data;

@Data
public class FreundeSuchenRequest {
    String suchString;
}
