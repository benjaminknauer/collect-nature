package ms.whg.collectnature.web;

import lombok.Data;

@Data
public class LoginRequest {
    String benutzername;
    String password;
}
