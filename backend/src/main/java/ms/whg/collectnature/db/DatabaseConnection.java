package ms.whg.collectnature.db;

import de.nrw.schulentwicklung.zaklassen.db.DatabaseConnector;
import de.nrw.schulentwicklung.zaklassen.db.DatabaseConnectorMysql;
import de.nrw.schulentwicklung.zaklassen.db.DatabaseConnectorSqlite;

public class DatabaseConnection {

    private static DatabaseConnector instance;

    public static DatabaseConnector getInstance() {
        if (instance == null) {
            String env = System.getenv("ENV");
            boolean useMysql = env != null && (env.equals("dev-prod") || env.equals("prod"));
            if (useMysql) {
                System.out.println("Creating MySQL connection.");
                instance = createDatabaseConnectorMysql();
            } else {
                System.out.println("Creating SQLite connection.");
                instance = createDatabaseConnectorSqlite();
            }
        }

        return instance;
    }

    public static void closeConnection() {
        if (instance == null) {
            return;
        }
        instance.close();
        instance = null;
    }

    private static DatabaseConnectorSqlite createDatabaseConnectorSqlite() {
        return new DatabaseConnectorSqlite(null, 0, "sqlite.db", null, null);
    }

    private static DatabaseConnectorMysql createDatabaseConnectorMysql() {
        return new DatabaseConnectorMysql(
                "playground-mysql.benjaminknauer.de",
                30000,
                "collect_nature",
                "root",
                "password"
        );
    }

}
