package ms.whg.collectnature.db;

import de.nrw.schulentwicklung.zaklassen.db.DatabaseConnector;
import de.nrw.schulentwicklung.zaklassen.db.QueryResult;
import ms.whg.collectnature.db.aggregate.FundEntityWithBenutzerDetails;
import ms.whg.collectnature.db.entity.FundEntity;

public class FundRepository {

    DatabaseConnector db = DatabaseConnection.getInstance();

    public FundEntity[] findAllByBenutzerId(int benutzerId) {
        String query = "SELECT * FROM fund WHERE benutzerId = " + benutzerId + " ORDER BY id DESC LIMIT 5;";
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        String[][] fundEntries = result.getData();
        FundEntity[] funde = new FundEntity[fundEntries.length];
        for (int i = 0; i < funde.length; i++) {
            int id = Integer.parseInt(fundEntries[i][0]);
            String bildUrl = fundEntries[i][1];
            String fundname = fundEntries[i][2];
            String beschreibung = fundEntries[i][3];
            String datum = fundEntries[i][4];
            String ort = fundEntries[i][5];
            int benutzerId1 = Integer.parseInt(fundEntries[i][6]);

            funde[i] = new FundEntity(id, bildUrl, fundname, beschreibung, datum, ort, benutzerId1);
        }

        return funde;
    }

    public FundEntity[] findAllByBenutzerIdPaginated(int benutzerId, int idOfLastDisplayedFund) {
        String query = "SELECT * FROM fund WHERE benutzerId = " + benutzerId
                + " AND id < " + idOfLastDisplayedFund
                + " ORDER BY id DESC LIMIT 5;";
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        String[][] fundEntries = result.getData();
        FundEntity[] funde = new FundEntity[fundEntries.length];
        for (int i = 0; i < funde.length; i++) {
            int id = Integer.parseInt(fundEntries[i][0]);
            String bildUrl = fundEntries[i][1];
            String fundname = fundEntries[i][2];
            String beschreibung = fundEntries[i][3];
            String datum = fundEntries[i][4];
            String ort = fundEntries[i][5];
            int benutzerId1 = Integer.parseInt(fundEntries[i][6]);

            funde[i] = new FundEntity(id, bildUrl, fundname, beschreibung, datum, ort, benutzerId1);
        }

        return funde;
    }

    public FundEntityWithBenutzerDetails[] findTimelineRelatedPaginated(int benutzerId, int idOfLastDisplayedFund) {
        String queryFormatString = """
                    SELECT
                    fund.id, fund.bildUrl, fund.fundname, fund.beschreibung, fund.datum, fund.ort, benutzer.benutzername, gefaellt.benutzerId as gefaellt
                    FROM fund
                             LEFT JOIN folgen ON fund.benutzerId = folgen.verfolgterBenutzerId
                             LEFT JOIN benutzer ON fund.benutzerId = benutzer.id
                             LEFT JOIN gefaellt ON fund.id = gefaellt.fundId AND folgen.benutzerId = gefaellt.benutzerId
                    WHERE folgen.benutzerId = %d AND fund.id < %d
                    ORDER BY id DESC
                    LIMIT 5;
                """;
        String query = String.format(queryFormatString, benutzerId, idOfLastDisplayedFund);
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        FundEntityWithBenutzerDetails[] fundEntitiesWithBenutzerDetails = new FundEntityWithBenutzerDetails[result.getRowCount()];
        for (int i = 0; i < result.getRowCount(); i++) {
            String[] resultRow = result.getData()[i];
            int id = Integer.parseInt(resultRow[0]);
            String bildUrl = resultRow[1];
            String fundname = resultRow[2];
            String beschreibung = resultRow[3];
            String datum = resultRow[4];
            String ort = resultRow[5];
            String benutzername = resultRow[6];
            boolean gefaellt = resultRow[7] != null;

            FundEntity fundEntity = new FundEntity(id, bildUrl, fundname, beschreibung, datum, ort, benutzerId);
            fundEntitiesWithBenutzerDetails[i] = new FundEntityWithBenutzerDetails(benutzerId, benutzername, fundEntity, gefaellt);
        }

        return fundEntitiesWithBenutzerDetails;
    }

    public FundEntityWithBenutzerDetails[] findTimelineRelated(int benutzerId) {
        String queryFormatString = """
                    SELECT
                    fund.id, fund.bildUrl, fund.fundname, fund.beschreibung, fund.datum, fund.ort, benutzer.benutzername, gefaellt.benutzerId as gefaellt 
                    FROM fund
                             LEFT JOIN folgen ON fund.benutzerId = folgen.verfolgterBenutzerId
                             LEFT JOIN benutzer ON fund.benutzerId = benutzer.id
                             LEFT JOIN gefaellt ON fund.id = gefaellt.fundId AND folgen.benutzerId = gefaellt.benutzerId
                    WHERE folgen.benutzerId = %d
                    ORDER BY id DESC
                    LIMIT 5;
                """;
        String query = String.format(queryFormatString, benutzerId);
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        FundEntityWithBenutzerDetails[] fundEntitiesWithBenutzerDetails = new FundEntityWithBenutzerDetails[result.getRowCount()];
        for (int i = 0; i < result.getRowCount(); i++) {
            String[] resultRow = result.getData()[i];
            int id = Integer.parseInt(resultRow[0]);
            String bildUrl = resultRow[1];
            String fundname = resultRow[2];
            String beschreibung = resultRow[3];
            String datum = resultRow[4];
            String ort = resultRow[5];
            String benutzername = resultRow[6];
            boolean gefaellt = resultRow[7] != null;

            FundEntity fundEntity = new FundEntity(id, bildUrl, fundname, beschreibung, datum, ort, benutzerId);
            fundEntitiesWithBenutzerDetails[i] = new FundEntityWithBenutzerDetails(benutzerId, benutzername, fundEntity, gefaellt);
        }

        return fundEntitiesWithBenutzerDetails;
    }

    public void insert(FundEntity fundEntity) {
        String id = fundEntity.getId() == null
                ? "NULL"
                : fundEntity.getId().toString();

        String query = String.format(
                "INSERT INTO fund VALUES (%s);",
                String.format("%s, '%s', '%s','%s','%s','%s', %d",
                        id,
                        fundEntity.getBildUrl(),
                        fundEntity.getFundname(),
                        fundEntity.getBeschreibung(),
                        fundEntity.getDatum(),
                        fundEntity.getOrt(),
                        fundEntity.getBenutzerId()
                )
        );

        db.executeStatement(query);
    }

}
