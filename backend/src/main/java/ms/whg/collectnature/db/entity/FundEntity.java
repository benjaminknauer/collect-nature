package ms.whg.collectnature.db.entity;

import lombok.Value;

@Value
public class FundEntity {
    Integer id;
    String bildUrl;
    String fundname;
    String beschreibung;
    String datum;
    String ort;
    int benutzerId;
}

