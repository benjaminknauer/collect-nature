package ms.whg.collectnature.db.aggregate;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class BenutzernameAndBenutzerId {
    String benutzername;
    int benutzerId;
}
