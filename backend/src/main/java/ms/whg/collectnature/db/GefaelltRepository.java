package ms.whg.collectnature.db;

import de.nrw.schulentwicklung.zaklassen.db.DatabaseConnector;
import de.nrw.schulentwicklung.zaklassen.db.QueryResult;

public class GefaelltRepository {

    DatabaseConnector db = DatabaseConnection.getInstance();

    public int getGefaelltCount(int pFundId) {
        String query = String.format(
                "SELECT COUNT(*) as gefaelltCount FROM gefaellt WHERE fundId = %d GROUP BY fundId;",
                pFundId
        );
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();
        if (result.getRowCount() == 0) {
            return 0;
        }

        return Integer.parseInt(result.getData()[0][0]);
    }

    public boolean isGefaellt(int benutzerId, int pFundId) {
        String query = String.format(
                "SELECT 1 FROM gefaellt WHERE benutzerId = %d AND fundId = %d;",
                benutzerId,
                pFundId
        );
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        return result.getRowCount() == 1;
    }

    public double getAvgBewertung(int pFundId) {
        String query = String.format(
                "SELECT AVG(bewertung) as avgBewertung FROM gefaellt WHERE fundId = %d AND bewertung;",
                pFundId
        );
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();
        if (result.getRowCount() == 0) {
            return 0;
        }
        if (result.getData()[0][0] == null) {
            return 0;
        }

        return Double.parseDouble(result.getData()[0][0]);
    }

    public int getBewertung(int pBenutzerId, int pFundId) {
        String query = String.format(
                "SELECT bewertung FROM gefaellt WHERE benutzerId = %d AND fundId = %d;",
                pBenutzerId,
                pFundId
        );
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();
        if (result.getRowCount() == 0) {
            return 0;
        }
        if (result.getData()[0][0] == null) {
            return 0;
        }

        return Integer.parseInt(result.getData()[0][0]);
    }

    public void insertGefaellt(int benutzerId, int pFundId, Integer pBewertung) {
        String bewertung = pBewertung == null ? "NULL" : pBewertung.toString();
        String query = String.format(
                "INSERT INTO gefaellt VALUES(%d, %d, %s);",
                benutzerId,
                pFundId,
                bewertung
        );
        db.executeStatement(query);
    }

    public void deleteGefaellt(int pBenutzerId, int pFundId) {
        String query = String.format(
                "DELETE FROM gefaellt WHERE benutzerId = %d AND fundId = %d;",
                pBenutzerId,
                pFundId
        );
        db.executeStatement(query);
    }

}
