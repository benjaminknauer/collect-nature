package ms.whg.collectnature.db;

import de.nrw.schulentwicklung.zaklassen.db.DatabaseConnector;
import de.nrw.schulentwicklung.zaklassen.db.QueryResult;

public class LikeRepository {

    DatabaseConnector db = DatabaseConnection.getInstance();

    public int getLikeCount(int pFundId) {
        String query = String.format(
                "SELECT COUNT(*) as likeCount FROM like WHERE fundId = %d GROUP BY fundId;",
                pFundId
        );
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();
        if (result.getRowCount() == 0) {
            return 0;
        }

        return Integer.parseInt(result.getData()[0][0]);
    }

    public boolean isLiked(int benutzerId, int pFundId) {
        String query = String.format(
                "SELECT 1 FROM like WHERE benutzerId = %d AND fundId = %d;",
                benutzerId,
                pFundId
        );
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        return result.getRowCount() == 1;
    }

    public void like(int benutzerId, int pFundId) {
        String query = String.format(
                "INSERT INTO like VALUES(%d, %d);",
                benutzerId,
                pFundId
        );
        db.executeStatement(query);
    }

    public void unlike(int pBenutzerId, int pFundId) {
        String query = String.format(
                "DELETE FROM like WHERE benutzerId = %d AND fundId = %d;",
                pBenutzerId,
                pFundId
        );
        db.executeStatement(query);
    }

}
