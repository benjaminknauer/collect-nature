package ms.whg.collectnature.db;

import de.nrw.schulentwicklung.zaklassen.db.DatabaseConnector;
import de.nrw.schulentwicklung.zaklassen.db.QueryResult;
import ms.whg.collectnature.db.entity.BenutzerEntity;
import ms.whg.collectnature.db.aggregate.BenutzernameAndBenutzerId;

import java.util.Optional;

public class BenutzerRepository {

    DatabaseConnector db = DatabaseConnection.getInstance();

    public Optional<BenutzerEntity> findByBenutzername(String pBenutzername) {
        String query = String.format("SELECT id, benutzername, passwort FROM benutzer where benutzername='%s';", pBenutzername);

        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        if (result.getRowCount() != 1) {
            return Optional.empty();
        }

        String[] benutzerRow = result.getData()[0];

        int benutzerId = Integer.parseInt(benutzerRow[0]);
        String benutzername = benutzerRow[1];
        String passwortHash = benutzerRow[2];

        return Optional.of(new BenutzerEntity(benutzerId, benutzername, passwortHash));
    }

    public Optional<BenutzerEntity> findById(int pId) {
        String query = String.format("SELECT id, benutzername, passwort FROM benutzer where id=%d;", pId);
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        if (result.getRowCount() != 1) {
            return Optional.empty();
        }

        String[] benutzerRow = result.getData()[0];

        int benutzerId = Integer.parseInt(benutzerRow[0]);
        String benutzername = benutzerRow[1];
        String passwortHash = benutzerRow[2];

        return Optional.of(new BenutzerEntity(benutzerId, benutzername, passwortHash));
    }

    public BenutzernameAndBenutzerId[] findAllByBenutzernameFuzzy(String pBenutzername, int pExcludeBenutzerId) {
        String query = "SELECT id, benutzername FROM benutzer WHERE benutzername LIKE '%" + pBenutzername + "%' AND NOT id = " + pExcludeBenutzerId + ";";
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        BenutzernameAndBenutzerId[] benutzerEntities = new BenutzernameAndBenutzerId[result.getRowCount()];
        for (int i = 0; i < result.getRowCount(); i++) {
            String[] benutzerRow = result.getData()[i];
            int benutzerId = Integer.parseInt(benutzerRow[0]);
            String benutzername = benutzerRow[1];
            benutzerEntities[i] = new BenutzernameAndBenutzerId(benutzername, benutzerId);
        }

        return benutzerEntities;
    }

    public void insertBenutzer(BenutzerEntity pBenutzer) {
        String query = String.format(
                "INSERT INTO benutzer(id, benutzername, passwort) VALUES (NULL, '%s', '%s')",
                pBenutzer.getBenutzername(),
                pBenutzer.getPasswort()
        );
        db.executeStatement(query);
    }

    public void folgen(int verfolgerId, int verfolgterId) {
        String query = String.format(
                "INSERT INTO folgen VALUES (%d, %d);",
                verfolgerId,
                verfolgterId
        );
        db.executeStatement(query);
    }

    public void entfolgen(int verfolgerId, int verfolgterId) {
        String query = String.format(
                "DELETE FROM folgen WHERE benutzerId = %d AND verfolgterBenutzerId = %d;",
                verfolgerId,
                verfolgterId
        );
        db.executeStatement(query);
    }

    public BenutzernameAndBenutzerId[] findFreunde(int pBenutzerId) {
        String query = String.format(
                "SELECT b.id, b.benutzername FROM folgen f JOIN benutzer b on f.verfolgterBenutzerId = b.id where f.benutzerId = %d;",
                pBenutzerId
        );
        db.executeStatement(query);
        QueryResult result = db.getCurrentQueryResult();

        BenutzernameAndBenutzerId[] freunde = new BenutzernameAndBenutzerId[result.getRowCount()];
        for (int i = 0; i < result.getRowCount(); i++) {
            String[] benutzerRow = result.getData()[i];
            int benutzerId = Integer.parseInt(benutzerRow[0]);
            String benutzername = benutzerRow[1];
            freunde[i] = new BenutzernameAndBenutzerId(benutzername, benutzerId);
        }

        return freunde;
    }

}
