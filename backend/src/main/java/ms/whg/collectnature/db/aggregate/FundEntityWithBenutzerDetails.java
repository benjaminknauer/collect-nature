package ms.whg.collectnature.db.aggregate;

import lombok.Value;
import ms.whg.collectnature.db.entity.FundEntity;

@Value
public class FundEntityWithBenutzerDetails {
    int benutzerId;
    String benutzername;
    FundEntity fund;
    boolean gefaellt;
}
