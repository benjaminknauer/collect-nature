package ms.whg.collectnature.db.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BenutzerEntity {
    Integer id;
    String benutzername;
    String passwort;
}
