package de.nrw.schulentwicklung.zaklassen.db;

public interface DatabaseConnector {

    /**
     * Der Auftrag schickt den im Parameter pSQLStatement enthaltenen SQL-Befehl an die
     * Datenbank ab.
     * Handelt es sich bei pSQLStatement um einen SQL-Befehl, der eine Ergebnismenge
     * liefert, so kann dieses Ergebnis anschließend mit der Methode getCurrentQueryResult
     * abgerufen werden.
     */
    void executeStatement(String pSQLStatement);

    /**
     * Die Anfrage liefert das Ergebnis des letzten mit der Methode executeStatement an
     * die Datenbank geschickten SQL-Befehls als Ob-jekt vom Typ de.nrw.schulentwicklung.zaklassen.db.mysql.de.nrw.schulentwicklung.zaklassen.db.QueryResult zurueck.
     * Wurde bisher kein SQL-Befehl abgeschickt oder ergab der letzte Aufruf von
     * executeStatement keine Ergebnismenge (z.B. bei einem INSERT-Befehl oder einem
     * Syntaxfehler), so wird null geliefert.
     */
    QueryResult getCurrentQueryResult();

    /**
     * Die Anfrage liefert null oder eine Fehlermeldung, die sich jeweils auf die letzte zuvor ausgefuehrte
     * Datenbankoperation bezieht.
     */
    String getErrorMessage();

    /**
     * Die Datenbankverbindung wird geschlossen.
     */
    void close();
}
