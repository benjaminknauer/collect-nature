-- MySQL dump 10.13  Distrib 8.0.30, for macos11.6 (x86_64)
--
-- Host: playground-mysql.benjaminknauer.de    Database: collect_nature
-- ------------------------------------------------------
-- Server version	5.6.51

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `benutzer`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `benutzer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `benutzername` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passwort` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bio` mediumtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_username_uindex` (`benutzername`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `benutzer`
--

/*!40000 ALTER TABLE `benutzer` DISABLE KEYS */;
INSERT INTO `benutzer` VALUES (1,'Thor','$2a$10$AdWLGgwG7jk29va6ySzQ5uWwf1Eq/mcuaJQ1/FG46aLaWmJB1fwti','null'),(2,'Pauker','$2a$10$Fg2q00zVf4InT01NbG3oqeIgrwg7d.uA/TDX78HRzKA3GL9Hy4mcm','null'),(3,'Pusteblume','$2a$10$UF1VlWi3zAxOOVCtRkCsauZQuRv0kXfd9KAwKhBE/7QQdNrVcGz.G','null'),(4,'MarleneMöller','$2a$10$xGmCMSrcQNG.N2OZt0UzNuLLLgJLf5Fb3fpxMHhV4z3WMSyMwBCbi','null'),(5,'jana.riehl@rocketmail.com','$2a$10$aYheososHfQu3aC2FEL19.pwWS8ZCY8Ff1hQ/7MrcokX8DgwidKOG','null'),(6,'Jonas','$2a$10$peiZzEZzhjFV/TM.gecTN./qtmTPpX.CvjyjNqRUETx9G5rPsVj06','null'),(7,'Peter','$2a$10$bDFBaRTUXmAthX5h1PuMIedPGCI1taP8dB5Zk.jAQ3/OvBZa30GAq','null'),(8,'marco_92','$2a$10$6uvzHMgAbm6I.rB7tdZGpOROjhfUmUHJWrFvSoetFq3lmlX55cy4O','null'),(9,'henni1310','$2a$10$L7BVjxf8hfhqJkslmQ/VlusS6hGnjcmx55DB1zvhFCcmCjo14hcOy','null'),(10,'ED','$2a$10$1fz9yGig.N3rsJc1IygndenZT6OCV8fcXY01Cn10YbHI9iDkD23Qa','null'),(11,'Max','$2a$10$ZgpJZ5sbZLS/.Yw4yDIXfOT41H85oZIoXmsVmSPzOU./lk8x6c6Uq','null');
/*!40000 ALTER TABLE `benutzer` ENABLE KEYS */;

--
-- Table structure for table `folgen`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `folgen` (
  `benutzerId` int(11) NOT NULL,
  `verfolgterBenutzerId` int(11) NOT NULL,
  PRIMARY KEY (`benutzerId`,`verfolgterBenutzerId`),
  KEY `folgen_benutzer_id_fk_2` (`verfolgterBenutzerId`),
  CONSTRAINT `folgen_benutzer_id_fk` FOREIGN KEY (`benutzerId`) REFERENCES `benutzer` (`id`),
  CONSTRAINT `folgen_benutzer_id_fk_2` FOREIGN KEY (`verfolgterBenutzerId`) REFERENCES `benutzer` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folgen`
--

/*!40000 ALTER TABLE `folgen` DISABLE KEYS */;
INSERT INTO `folgen` VALUES (2,1),(6,1),(1,2),(3,2),(6,2),(8,2),(2,3),(2,4),(2,5),(2,6),(4,6),(2,7),(3,7),(6,7),(2,8),(2,9),(2,10);
/*!40000 ALTER TABLE `folgen` ENABLE KEYS */;

--
-- Table structure for table `fund`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bildUrl` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `beschreibung` longtext COLLATE utf8mb4_unicode_ci,
  `datum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ort` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `benutzerId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fund_benutzer_id_fk` (`benutzerId`),
  CONSTRAINT `fund_benutzer_id_fk` FOREIGN KEY (`benutzerId`) REFERENCES `benutzer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fund`
--

/*!40000 ALTER TABLE `fund` DISABLE KEYS */;
INSERT INTO `fund` VALUES (1,'/user-photos/1/5cadc803-4623-4a68-8645-908552c74f90_Daucus3.jpeg','Wilde Möhre (Daucus carota)','Sehr viele am Hafen entdeckt!','2022-06-27 14:10','Münster',1),(2,'/user-photos/2/b59da6ae-d167-4994-b877-444918becf03_1658927386261.jpg','Schwarzäugige Susanne','','2022-06-27 15:14','Münster',2),(3,'/user-photos/2/42bb0398-392c-4c9d-9b39-2fdf5eca9453_markup_thumb_1657974932953.jpg.jpg.png','Wiesen-Witwenblume','Schöner Fund im Urlaub 🐝','2022-06-19 15:32','Etretat',2),(4,'/user-photos/2/430754f9-ab5a-4e16-be2f-977876b34b78_markup_thumb_1657916238458.jpg.jpg.png','Acker-Kratzdistel','','2022-06-15 15:36','Yport (Frankreich)',2),(5,'/user-photos/2/f6c8a6db-c585-48f8-b2dd-fdfd8b2f93b5_1658932220361.jpg','Japanischer Flügelknöterich','Überall an den Gleisen. Leider eine Invasive Art.','2022-06-27 16:31','Münster',2),(6,'/user-photos/2/84529b5f-0022-4e28-8142-644ea18d0338_PXL_20220727_143343766.MP.jpg','Spitz-Ahorn','Zu erkennen an den spitzen Blättern ...','2022-06-27 16:33','Münster',2),(7,'/user-photos/2/caa7997f-6fe9-4111-ae88-1a0c0547bb49_1658932535807.jpg','Hunds-Rose','','2022-06-27 16:35','Münster',2),(8,'/user-photos/6/6af8e7d1-0e59-4853-bd91-f76e46b3053d_image.jpg','Salatrauke (Ruca)','Auch als Rucola bekannt.','2022-06-28 11:40','Balkonien',6),(9,'/user-photos/6/ec71c2f1-97ac-4601-bff8-3cfe2a51fcf6_image.jpg','Aleovera','','2022-06-28 11:48','Nasszellien ',6),(10,'/user-photos/6/2de35717-48a1-4211-8b5c-625a626398e8_image.jpg','Kohlrabi Azur Star','Noch ca 4 Wochen bis zur Ernte','2022-06-28 12:11','Balkonien',6),(12,'/user-photos/6/08c2d64e-1c44-476b-9dbf-07d8f89ca636_image.jpg','Dreikantige Wolfsmilch (Euphorbia trigona)','Alter: 25 Jahre','2022-06-28 12:14','Wohnzimmer',6),(14,'/user-photos/6/1790149f-0495-4caf-9e93-db6bc9305c69_image.jpg','Monstera','','2022-06-28 12:20','Wohnzimmer',6),(15,'/user-photos/6/6068ab0c-a718-48af-b726-b7e1cca8ee52_image.jpg','Brombeere (Rubus)','','2022-06-28 12:20','Münster',6),(16,'/user-photos/6/9f11ef2b-d696-49bd-b788-a6e275f5c972_image.jpg','Gefleckter Aronstab (Arum maculatum)','','2022-06-28 12:23','Münster',6),(17,'/user-photos/6/a7bc66d7-3880-4f0f-a646-777bfe5d1c88_image.jpg','Brennnessel (Urtica)','','2022-06-28 12:26','Münster',6),(18,'/user-photos/6/d9feba46-a063-495e-9c9c-15b1faa22737_image.jpg','Birke (Betula)','','2022-06-28 12:27','Münster',6),(19,'/user-photos/6/8bee1ba0-78ac-4e31-a583-f8d8d6413785_image.jpg','Eiche (Quercus)','','2022-06-28 12:29','Münster',6),(20,'/user-photos/6/d9cb7232-ead3-4784-909f-a082e4a3a987_image.jpg','Zucchini ','','2022-06-28 12:31','Münster',6),(21,'/user-photos/6/d6130b27-99f0-4a80-9e2f-41f32fc80783_image.jpg','Mangold','Wie Salat/Spinat essbar','2022-06-28 12:32','Münster',6),(22,'/user-photos/6/5a805b4a-3977-4597-9003-5ed93b0b2b5c_8B4268A2-80AB-476A-9A59-F52ABE89A16A.jpeg','Blaubeere (Vaccinium myrtillus)','','2022-06-28 12:39','Bad Schandau',6),(24,'/user-photos/6/acb48e2e-fc87-407d-af91-13263c8a9511_085DD5E2-E357-42C6-8F8F-1EC31E0080B2.jpeg','Buche','','2022-06-28 12:41','Altendorf Sebnitz',6),(25,'/user-photos/6/326b05d0-dce9-4bc3-82ec-032cbf39ea54_image.jpg','Chinesischer Geldbaum (Pilea peperomioides)','','2022-06-28 13:34','Münster',6),(26,'/user-photos/7/5fdc1626-532c-4444-97a1-f2fd1dce9358_IMG-20220727-WA0038.jpg','Enzian','','2022-06-28 14:13','Osttirol',7),(27,'/user-photos/7/098f95cd-3c91-4eab-bb95-5dac1d6b2bf9_IMG-20220727-WA0016.jpg','Alpenrose','','2022-06-28 14:14','Osttirol',7),(28,'/user-photos/7/adc2056f-a2d1-40bb-9069-d976a4928cd0_IMG-20220724-WA0024.jpeg','Hibiskus','','2022-06-28 14:14','Gütersloh',7),(29,'/user-photos/7/805e7c9f-59d3-45ed-8546-d365e1b55938_IMG-20220715-WA0001.jpeg','Knallerbe','','2022-06-28 14:16','Münster',7),(30,'/user-photos/7/71171fc7-f025-4eed-a3cb-b93de39255bb_IMG-20220709-WA0173.jpg','Giftiger Aaronstab','','2022-06-28 14:16','Frankreich, Auvergne',7),(31,'/user-photos/3/735aa973-2120-4aa3-96d7-8b524d2fc294_20220728_154714.jpg','Feige','','2022-06-28 15:37','Münster ',3),(32,'/user-photos/3/d870d752-ddb0-4817-89d5-c822d679c553_20220728_152156.jpg','Cola-Kraut','','2022-06-28 15:49','Münster ',3),(33,'/user-photos/3/00678063-c9b3-41b7-aedf-6698aa0e0dd4_1659016486089722069058683211768.jpg','Lavendel','','2022-06-28 15:54','Münster ',3),(34,'/user-photos/2/e79615a8-1007-4db8-bd14-9d355069345f_markup_thumb_1654954734896.jpg.jpg.png','Fächer-Ahorn (Acer palmatum)','','2022-06-11 16:07','Münster - Staufenbergstraße',2),(35,'/user-photos/2/3d238971-ecef-480d-ae4e-4ad5974ad670_1659019003582.jpg','Dreilappige Jungfernrebe (Parthenocissus tricuspidata)','Die komplette Fassade ist voll damit 🏠🪴','2022-06-28 16:37','Münster',2),(36,'/user-photos/2/02d651fe-990b-4660-8ccf-a7e0fa8b8870_PXL_20220728_144201503.jpg','Rispen-Blasenbaum (Koelreuteria paniculata)','Auch Blasenesche genannt. Ursprünglich aus China.','2022-06-28 16:42','Münster',2),(37,'/user-photos/2/3ae0832f-e363-4b19-a16b-e1444923097f_PXL_20220728_144613498.jpg','Wilde Malve (Malva sylvestris)','Wissenswert: Blüten und Blätter enthalten Schleim, der Reizhusten lindert. Galt im 16. Jahrhundert als Allheilmittel.','2022-06-28 16:47','Münster',2),(38,'/user-photos/9/65152ab6-9dbf-43cb-8542-c28461ba0616_9252D288-BAC6-4E80-A5EC-91CB9B922F03.jpeg','Hortensie','','2022-06-28 17:04','Münster',9),(39,'/user-photos/9/a8a4b6b4-9d2e-43ec-b106-34615d3c8d07_3661E1DC-A7F2-4E4C-80D6-6137BD8A25C5.jpeg','Goldfruchtpalme','','2022-06-28 17:07','Münster',9),(40,'/user-photos/9/8fe8e421-7995-449e-b89a-0a601a3c048d_C6949E77-C98C-4057-8FD5-1637534A1824.jpeg','Monstera','','2022-06-28 17:08','Münster',9),(41,'/user-photos/9/e544479e-5b51-4127-ad9c-7a8122cb00e5_C2769B21-A50A-4585-AE24-B34F0006D639.jpeg','Pilea','','2022-06-28 17:09','Münster',9),(42,'/user-photos/9/dd8e8eb7-3eb0-46c5-955d-31bf108cfb79_E1BD535F-9734-48CD-AF36-A204FEFB4ECA.jpeg','Schopf-Lavendel','','2022-06-28 17:12','Münster',9),(43,'/user-photos/9/bb7034f3-c082-4ea9-9c53-8c5ae96d002e_C4DF7FB9-FF4C-425C-92D5-D0C15D8CDDAA.jpeg','Eukalyptus','','2022-06-28 17:13','Münster',9),(44,'/user-photos/9/52b8f311-11ed-4327-9b41-99eafda65e3a_BF9B9BCF-9C85-407A-822B-F0711120545A.jpeg','Kanarische Dattelpalme','','2022-06-28 17:14','Münster',9),(45,'/user-photos/3/d4ea3435-08db-4b00-bb9e-f66e9a6d8eba_20220728_153644.jpg','Tomate','','2022-06-28 17:18','Münster ',3),(46,'/user-photos/9/d89e34a2-d553-4128-84cf-b50671447731_0B7E1B60-009B-4125-9CAE-8A113146333B.jpeg','Geldbaum','','2022-06-28 17:19','Münster',9),(47,'/user-photos/3/fb8995ae-5679-47f7-bb91-e4a6bb4892f7_20220728_153633.jpg','Kohlrabi ','','2022-06-28 17:19','Münster',3),(48,'/user-photos/3/db3ca73e-d4ba-4080-b06a-f86106e00988_20220728_152129.jpg','Salbei','','2022-06-28 17:19','Münster',3),(49,'/user-photos/3/82c3f98c-cd18-42ed-ac9a-c7fdc0a549cd_20220728_154638.jpg','Stockrose','','2022-06-28 17:20','Münster',3),(50,'/user-photos/3/8a3a9e0f-4060-470b-a958-d735120559f3_20220728_154627.jpg','Ringelblume','','2022-06-28 17:21','Münster',3),(51,'/user-photos/3/80d718bc-65ae-4b09-9d55-eae7d0bddace_IMG-20170611-WA0006.jpg','Kirschbaum','','2022-06-28 17:23','Unna',3),(52,'/user-photos/10/20412d41-196b-4a8d-a79f-046c5235eb06_761E0D02-4D46-4565-84A6-16E4B50A27C0.jpeg','Sabila Aloe Vera','','2022-06-28 20:37','Münster',10),(53,'/user-photos/10/a9072d02-649b-46f0-bdf6-5e7b3db00ce0_00C02431-B73F-4A0C-9974-1AF9FB47BC08.jpeg','Ficus microcatpa ginseng','','2022-06-28 20:40','Münster',10),(54,'/user-photos/10/d2a3878c-886e-4d4f-8390-d11e4e2e626c_872D8B3B-3A7D-484D-A4B1-B74781FD86E2.jpeg','Embeleso','','2022-06-28 20:40','Münster ',10),(55,'/user-photos/10/79a60793-4d00-4abe-b2c7-80819d4e2e6d_50725DE5-0FF0-4B2F-A20B-87C664D99287.jpeg','Fuchsie','','2022-06-28 20:41','Münster',10),(56,'/user-photos/10/1fa3de71-0d29-405f-99dc-0b65d54766ca_0B969332-E781-46CA-A590-4440864FF649.jpeg','Schmucklilie','','2022-06-28 20:42','Münster',10),(57,'/user-photos/10/c7e78151-8849-4891-9dae-37a51c57a9b8_EBA70999-C1C3-4D49-A3B1-36920291ED62.jpeg','Elefantenfuß (Beaucarnea recurvata)','','2022-06-28 20:43','Münster',10);
/*!40000 ALTER TABLE `fund` ENABLE KEYS */;

--
-- Table structure for table `gefaellt`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gefaellt` (
  `benutzerId` int(11) NOT NULL,
  `fundId` int(11) NOT NULL,
  PRIMARY KEY (`benutzerId`,`fundId`),
  KEY `like_fund_id_fk` (`fundId`),
  CONSTRAINT `like_benutzer_id_fk` FOREIGN KEY (`benutzerId`) REFERENCES `benutzer` (`id`),
  CONSTRAINT `like_fund_id_fk` FOREIGN KEY (`fundId`) REFERENCES `fund` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gefaellt`
--

/*!40000 ALTER TABLE `gefaellt` DISABLE KEYS */;
INSERT INTO `gefaellt` VALUES (2,1),(2,3),(8,7),(2,8),(6,8),(2,16),(6,16),(2,18),(2,24),(6,24),(2,26),(6,26),(2,27),(6,27),(2,31),(2,37),(2,49),(2,50),(2,56);
/*!40000 ALTER TABLE `gefaellt` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-31 16:35:51
