import { useRouter } from 'vue-router';

export function useFetchWrapper() {
  const router = useRouter();

  async function get(url, options) {
    return fetchWithAuthHandling(url, { ...options, method: 'GET' });
  }

  async function post(url, payload, options) {
    return fetchWithAuthHandling(
      url,
      { ...options, method: 'POST', body: JSON.stringify(payload) },
      router
    );
  }

  async function fetchWithAuthHandling(url, options) {
    const authToken = localStorage.getItem('authorization-token');
    const response = await fetch(process.env.API_URL + url, {
      ...options,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: `Bearer ${authToken}`,
        ...options.headers,
      },
    });

    if (!response.ok && response.status === 401) {
      await router.push('/login');
    }

    return response;
  }

  return { get, post };
}
