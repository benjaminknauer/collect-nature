export function getAvatarColor(username) {
  const startletter = username.substring(0, 1).toUpperCase();
  switch (startletter) {
    case 'A':
      return 'red-10';
    case 'B':
      return 'pink-10';
    case 'C':
      return 'purple-10';
    case 'D':
      return 'deep-purple-10';
    case 'E':
      return 'indigo-10';
    case 'F':
      return 'blue-10';
    case 'G':
      return 'light-blue-10';
    case 'H':
      return 'cyan-7';
    case 'I':
      return 'cyan-10';
    case 'J':
      return 'teal-7';
    case 'K':
      return 'teal-10';
    case 'L':
      return 'green-6';
    case 'M':
      return 'green-10';
    case 'N':
      return 'light-green-7';
    case 'O':
      return 'light-green-10';
    case 'P':
      return 'lime-7';
    case 'Q':
      return 'lime-10';
    case 'R':
      return 'yellow-7';
    case 'S':
      return 'yellow-10';
    case 'T':
      return 'amber-10';
    case 'V':
      return 'organge-10';
    case 'W':
      return 'deep-organe-10';
    case 'X':
      return 'brown-10';
    case 'Y':
      return 'grey-10';
    case 'Z':
    default:
      return 'blue-grey-10';
  }
}
