import { RouteRecordRaw } from 'vue-router';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/NotLoggedInLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/IndexPage.vue') },
      { path: '/register', component: () => import('pages/RegisterPage.vue') },
      { path: '/login', component: () => import('pages/LoginPage.vue') },
      {
        path: '/datenschutz',
        component: () => import('pages/DatenschutzPage.vue'),
      },
      {
        path: '/impressum',
        component: () => import('pages/ImpressumPage.vue'),
      },
    ],
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/timeline', component: () => import('pages/TimelinePage.vue') },
      { path: '/new', component: () => import('pages/NeuerFundPage.vue') },
      {
        path: '/funde/:benutzerId',
        component: () => import('pages/FundePage.vue'),
      },
      { path: '/freunde', component: () => import('pages/FreundePage.vue') },
      { path: '/leaderboard', component: () => import('pages/LeaderboardPage.vue') },
      {
        path: '/datenschutz',
        component: () => import('pages/DatenschutzPage.vue'),
      },
      {
        path: '/impressum',
        component: () => import('pages/ImpressumPage.vue'),
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue'),
  },
];

export default routes;
