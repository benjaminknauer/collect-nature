# Collect Nature! - Das Social Network für Naturbegeisterte.

*Eine Webapp für den Unterricht im Leistungskurs Informatik zum Thema __Datenbanken__.*

Erreichbar unter: https://collect-nature.benjaminknauer.de

### Local Development

### Requirements

- Open-JDK (Version 18)
- maven
- node (Version >=16)

### Backend starten

*Aktueller Build des Frontends wird mitgestartet.*

- __IntelliJ__ `RunApplication` oder
- __Maven__  
  *Achtung: sqlite.db muss dann im backend/ Ordner liegen.*
  ```
  cd backend
  mvn spring-boot:run 
  ```

Browser: http://localhost:8080

### Frontend starten

*Backend muss unter http://localhost:8080 erreichbar sein. Siehe oben.*

```
cd frontend
npm run dev
```

Browser: http://localhost:9000

### Datenbank (local)

File: `sqllite.sb`  
Schema: `migrations-db/sqlite`

### Datenbank (prod)

Adresse: `playground-mysql.benjaminknauer.de`  
Port: `30000`  
User: `root`  
Passwort: `********`  
Schema: `migrations-db/mysql`  
*Erreichbar über cloud-beaver: https://beaver.benjaminknauer.de*

### Build

```
mvn clean package
docker build .
```

### Deployment

Automatisches Deployment auf k8s-Cluster.

```
git add -A              # Änderungen zum Staging hinzufügen
git status              # Änderungen überprüfen
git commit -m "changes" # Änderungen committen
git push                # Commit pushen
git tag "0.1.0"         # Neuen Versionstag anlegen
git push --tags         # Tags pushen
```

Prod Instanz nach wenigen Minuten erreichbar unter: https://collect-nature.benjaminknauer.de
